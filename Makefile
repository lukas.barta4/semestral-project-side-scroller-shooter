CXX = g++
CXXFLAGS =-std=c++17 -Wall -pedantic -Wno-long-long
all: compile doc

run: compile
	./bartalu3
	
doc:
	doxygen doxygen/doxconfg


compile: build/main.o build/Menu.o build/GameData.o build/GameScene.o  build/Object.o build/GameObject.o build/Projectile.o build/Entity.o build/Player.o build/Enemy.o build/Weapon.o build/CollisionVisitor.o build/Bonus.o build/ScoreManager.o build/LevelManager.o
	$(CXX)  $(CXXFLAGS) build/main.o build/Menu.o -lstdc++fs build/GameData.o build/GameScene.o build/Object.o build/GameObject.o build/Projectile.o build/Entity.o build/Player.o build/Enemy.o build/Weapon.o build/CollisionVisitor.o build/Bonus.o build/ScoreManager.o build/LevelManager.o -o bartalu3 -lncurses
	
build/main.o: src/main.cpp
	$(CXX)  $(CXXFLAGS) -c src/main.cpp -o build/main.o
	
build/Menu.o: src/Menu.h src/Menu.cpp
	$(CXX)  $(CXXFLAGS) -c src/Menu.cpp -lstdc++fs  -o build/Menu.o
	
build/GameData.o: src/GameData.h src/GameData.cpp
	$(CXX)  $(CXXFLAGS) -c src/GameData.cpp -o build/GameData.o
	
build/GameScene.o: src/GameScene.h src/GameScene.cpp
	$(CXX)  $(CXXFLAGS) -c src/GameScene.cpp -o build/GameScene.o
	
build/Object.o: src/Object.h src/Object.cpp
	$(CXX)  $(CXXFLAGS) -c src/Object.cpp -o build/Object.o
	
build/GameObject.o: src/GameObject.h src/GameObject.cpp
	$(CXX)  $(CXXFLAGS) -c src/GameObject.cpp -o build/GameObject.o
	
build/Projectile.o: src/Projectile.h src/Projectile.cpp
	$(CXX)  $(CXXFLAGS) -c src/Projectile.cpp -o build/Projectile.o
	
build/Entity.o: src/Entity.h src/Entity.cpp
	$(CXX)  $(CXXFLAGS) -c src/Entity.cpp -o build/Entity.o
	
build/Player.o: src/Player.h src/Player.cpp
	$(CXX)  $(CXXFLAGS) -c src/Player.cpp -o build/Player.o
	
build/Enemy.o: src/Enemy.h src/Enemy.cpp
	$(CXX)  $(CXXFLAGS) -c src/Enemy.cpp -o build/Enemy.o
	
build/Weapon.o: src/Weapon.h src/Weapon.cpp
	$(CXX)  $(CXXFLAGS) -c src/Weapon.cpp -o build/Weapon.o
	
build/CollisionVisitor.o: src/CollisionVisitor.h src/CollisionVisitor.cpp
	$(CXX)  $(CXXFLAGS) -c src/CollisionVisitor.cpp -o build/CollisionVisitor.o
	
build/Bonus.o: src/Bonus.h src/Bonus.cpp
	$(CXX)  $(CXXFLAGS) -c src/Bonus.cpp -o build/Bonus.o
	
build/ScoreManager.o: src/ScoreManager.h src/ScoreManager.cpp
	$(CXX)  $(CXXFLAGS) -c src/ScoreManager.cpp -o build/ScoreManager.o
	
build/LevelManager.o: src/LevelManager.h src/LevelManager.cpp
	$(CXX)  $(CXXFLAGS) -c src/LevelManager.cpp -o build/LevelManager.o

.PHONY: clean

clean:
	rm -f build/*.o
	rm -rf doc
	rm -f bartalu3
