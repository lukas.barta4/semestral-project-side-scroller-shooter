**Space fighter 2020**

![](ll6aaMM.png)

**Requirements:**
- Runs only on linux
- c++17
- ncurses
- doxygen and graphviz (for generating documentation)

**Commands**


- make all - compiles code and generates documentation if needed
- make clean - deletes all compiled/generated files
- make run - compiles code if needed and runs game
- make doc - generates documentation


