#include "Bonus.h"
#include "CollisionVisitor.h"
#include "GameData.h"
#include "Player.h"
#include "Weapon.h"
Bonus::Bonus(const int & x, const int & y){
    SetXY(x,y);
    SetMovementVector(-1,0);
    SetBoundBox(0,0);
    m_MovementSpeed = GameData::m_BonusSpeed;
}

void Bonus::Update() {
    ObjectMove();
}
void Bonus::Visit (CollisionVisitor * visitor) {
    visitor->VisitBonus(this);
}
void Bonus::CollideWith (GameObject * collisionObject){
    CollisionBonus visitor{this};
    collisionObject->Visit(&visitor);
}

RocketBonus::RocketBonus(const int & x, const int & y):Bonus(x,y) {
    SetSprite("R\n");
}

void RocketBonus::ApplyBonus(Player * player){
    player->UpgradeRocket();
}

CannonBonus::CannonBonus(const int & x, const int & y):Bonus(x,y) {
    SetSprite("C\n");
}

void CannonBonus::ApplyBonus(Player * player){
    player->UpgradeCanon();
}

MachineGunBonus::MachineGunBonus(const int & x, const int & y):Bonus(x,y) {
    SetSprite("M\n");
}

void MachineGunBonus::ApplyBonus(Player * player){
    player->UpgradeMachineGun();
}
HealBonus::HealBonus(const int & x, const int & y):Bonus(x,y) {
    SetSprite("H\n");
}
void HealBonus::ApplyBonus(Player * player) {
    player->Heal();
}