#ifndef Bonus_H
#define Bonus_H


#include "GameObject.h"
class CollisionVisitor;
class Player;
/**
 * @brief Abstract class representing bonuses that can be applied to Player
 * 
 */
class Bonus: public GameObject{
public:
    /**
     * @brief Construct a new Bonus object for given coordinates
     * 
     * @param x int X coordinate
     * @param y int Y coordinate
     */
    Bonus(const int & x, const int & y);

    void Visit (CollisionVisitor * visitor) override;
    void CollideWith (GameObject * collisionObject) override;

    /**
     * @brief Pure virtual function to apply bonus modifier to player
     * 
     * @param player 
     */
    virtual void ApplyBonus(Player * player)=0;
    /**
     * @brief Update to move object
     * 
     */
    void Update() override;
};

/**
 * @brief RocketLauncher weapon bonus
 * 
 */
class RocketBonus : public Bonus{
    public:
    RocketBonus(const int & x, const int & y);
    void ApplyBonus(Player * player)override;
};

/**
 * @brief Cannon weapon bonus
 * 
 */
class CannonBonus : public Bonus{
    public:
    CannonBonus(const int & x, const int & y);
    void ApplyBonus(Player * player)override;
};

/**
 * @brief MachineGun weapon bonus
 * 
 */
class MachineGunBonus : public Bonus{
    public:
    MachineGunBonus(const int & x, const int & y);
    void ApplyBonus(Player * player)override;
};

/**
 * @brief Health repairing bonus
 * 
 */
class HealBonus : public Bonus{
    public:
    HealBonus(const int & x, const int & y);
    void ApplyBonus(Player * player)override;
};

#endif /* Bonus_H */