#include "CollisionVisitor.h"


void CollisionPlayer::VisitEnemy(Enemy * enemy) {
    m_Player->Damage(1);
    if(!enemy->isMarked()){
        enemy->BonusRoll();
    }
    enemy->Destroy();
}

void CollisionPlayer::VisitProjectile(Projectile * projectile) {
    if(projectile->GetMoveDirHorizontal() == -1){
        m_Player->Damage(1);
        projectile->Destroy();
    }
    
}

void CollisionPlayer::VisitPlayer(Player * player) {}

void CollisionPlayer::VisitBonus(Bonus * bonus) {
    bonus->ApplyBonus(m_Player);
    bonus->Destroy();
}

void CollisionEnemy::VisitEnemy(Enemy * enemy) {}

void CollisionEnemy::VisitProjectile(Projectile * projectile) {
    if(projectile->GetMoveDirHorizontal()==1){
        m_Enemy->Damage(projectile->GetDmg());
        projectile->Destroy();
    }
}

void CollisionEnemy::VisitPlayer(Player * player) {
    player->Damage(1);
    if(!m_Enemy->isMarked()){
        m_Enemy->BonusRoll();
    }
    m_Enemy->Destroy();
}

void CollisionEnemy::VisitBonus(Bonus * bonus) {}

void CollisionBonus::VisitEnemy(Enemy * enemy) {}

void CollisionBonus::VisitProjectile(Projectile * projectile) {}

void CollisionBonus::VisitPlayer(Player * player) {
    m_Bonus->ApplyBonus(player);
    m_Bonus->Destroy();
}

void CollisionBonus::VisitBonus(Bonus * bonus) {}

void CollisionProjectile::VisitEnemy(Enemy * enemy) {
    if(m_Projectile->GetMoveDirHorizontal()==1){
        enemy->Damage(m_Projectile->GetDmg());
        m_Projectile->Destroy();
    }
}

void CollisionProjectile::VisitProjectile(Projectile * projectile) {
    if(projectile->GetMoveDirHorizontal() == m_Projectile->GetMoveDirHorizontal()){
        return;
    }
    projectile->Destroy();
    m_Projectile->Destroy();
}

void CollisionProjectile::VisitPlayer(Player * player) {
    if(m_Projectile->GetMoveDirHorizontal() == -1){
        player->Damage(1);
        m_Projectile->Destroy();
    }
}

void CollisionProjectile::VisitBonus(Bonus * bonus) {}
