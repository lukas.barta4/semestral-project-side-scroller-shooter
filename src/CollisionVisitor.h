#ifndef CollisionVisitor_H
#define CollisionVisitor_H

#include "Player.h"
#include "Enemy.h"
#include "Projectile.h"
#include "Bonus.h"
class Player;
class Enemy;
class Projectile;
class Bonus;


/**
 * @brief Abstract class of double dispatch visitor design patter
 * 
 */
class CollisionVisitor{
    public:
    /**
     * @brief Gets Enemy pointer from visiting object
     * 
     */
    virtual void VisitEnemy(Enemy * enemy) = 0;

    /**
     * @brief Gets Projectile pointer from visiting object
     * 
     */
    virtual void VisitProjectile(Projectile * projectile) = 0;

    /**
     * @brief Gets Player pointer from visiting object
     * 
     */
    virtual void VisitPlayer(Player * player) = 0;

    /**
     * @brief  Gets Bonus pointer from visiting object
     * 
     */
    virtual void VisitBonus(Bonus * bonus) = 0;
};
/**
 * @brief Resolves collision between Player and other objects
 * 
 */
class CollisionPlayer: public CollisionVisitor{
    public:
    /**
     * @brief Construct a new Collision Player object with Player pointer 
     * 
     */
    CollisionPlayer (Player * player) : m_Player{player}{}

    void VisitEnemy(Enemy * enemy) override;
    void VisitProjectile(Projectile * projectile) override;
    void VisitPlayer(Player * player) override;
    void VisitBonus(Bonus * bonus) override;
    private:
    Player * m_Player = nullptr;
};

/**
 * @brief Resolves collision between Enemy and other objects
 * 
 */
class CollisionEnemy: public CollisionVisitor{
    public:
    /**
     * @brief Construct a new Collision Enemy object with Enemy pointer
     * 
     */
    CollisionEnemy (Enemy * enemy) : m_Enemy{enemy}{}

    void VisitEnemy(Enemy * enemy) override;
    void VisitProjectile(Projectile * projectile) override;
    void VisitPlayer(Player * player) override;
    void VisitBonus(Bonus * bonus) override;
    private:
    Enemy * m_Enemy = nullptr;
};

/**
 * @brief Resolves collision between Bonus and other objects
 * 
 */
class CollisionBonus: public CollisionVisitor{
    public:
    /**
     * @brief Construct a new Collision Bonus object with Bonus pointer
     * 
     */
    CollisionBonus (Bonus * bonus) : m_Bonus{bonus}{}

    void VisitEnemy(Enemy * enemy) override;
    void VisitProjectile(Projectile * projectile) override;
    void VisitPlayer(Player * player) override;
    void VisitBonus(Bonus * bonus) override;
    private:
    Bonus * m_Bonus = nullptr;
};

/**
 * @brief Resolves collision between Projectile and other objects
 * 
 */
class CollisionProjectile: public CollisionVisitor{
    public:
    /**
     * @brief Construct a new Collision Projectile object with Projectile pointer
     * 
     */
    CollisionProjectile (Projectile * projectile) : m_Projectile{projectile}{}

    void VisitEnemy(Enemy * enemy) override;
    void VisitProjectile(Projectile * projectile) override;
    void VisitPlayer(Player * player) override;
    void VisitBonus(Bonus * bonus) override;
    private:
    Projectile * m_Projectile = nullptr;
};

#endif /* CollisionVisitor_H */