#include "Enemy.h"
#include "GameScene.h"
#include "GameData.h"
#include "Bonus.h"
#include "Weapon.h"
#include "CollisionVisitor.h"
#include <stdlib.h>     
#include <time.h> 

Enemy::Enemy(const int & x,const int & y){
    SetXY(x,y);
}

void Enemy::Damage(const int & dmg) {
    m_Health -= dmg ;
    if (m_Health <= 0){
        GameScene::GetInstance().AddScore(m_ScoreValue);
        BonusRoll();
        Destroy();
    }
}

void Enemy::BonusRoll() {
    //roll number for chance to spawn bonus
    if((rand() % 100 <= GameData::m_BonusChance)){
        //chooses random bonus
        int randBonus = rand() % 4 + 1;
        switch (randBonus)
        {
        case 1:
            GameScene::GetInstance().AddObject(new RocketBonus(m_X + GetBoundHeight()/2,m_Y + GetBoundWidth()/2));
            break;
        case 2:
            GameScene::GetInstance().AddObject(new CannonBonus(m_X + GetBoundHeight()/2,m_Y + GetBoundWidth()/2));
            break;
        case 3:
            GameScene::GetInstance().AddObject(new MachineGunBonus(m_X + GetBoundHeight()/2,m_Y + GetBoundWidth()/2));
            break;
        case 4:
            GameScene::GetInstance().AddObject(new HealBonus(m_X + GetBoundHeight()/2,m_Y + GetBoundWidth()/2));
            break;
        }
    }

}

void Enemy::Update() {
    ObjectMove();
    m_Weapon->Update();
}

void Enemy::Visit (CollisionVisitor * visitor) {
    visitor->VisitEnemy(this);
}

void Enemy::CollideWith (GameObject * collisionObject){
    CollisionEnemy visitor{this};
    collisionObject->Visit(&visitor);
}

Fighter::Fighter(const int & x,const int & y):Enemy(x,y) {
    SetupStats();
}

void Fighter::SetupStats(){
    SetSprite(" /|\n"
              "<H<\n"
              " \\|\n");
    m_ScoreValue = GameData::m_FighterScoreValue;
    m_Health = GameData::m_FighterHealth;
    m_MovementSpeed = GameData::m_FighterSpeed;
    SetBoundBox(2,2);
    SetMovementVector(-1,0);    
    InitWeapon();
}
void Fighter::InitWeapon(){
    m_Weapon = new MachineGun(m_X,m_Y);
    m_Weapon->SetOffsetXY(-1,1);
    m_Weapon->SetDirection(-1);
    m_Weapon->SetLevel(2);
    m_Weapon->SetFireRate(GameData::m_FighterFireRate);
}

Bomber::Bomber(const int & x,const int & y):Enemy(x,y) {
    SetupStats();
}


void Bomber::SetupStats(){
    SetSprite(" /H\\\n"
		      "<###{\n"
		      " \\H/\n");
    m_ScoreValue = GameData::m_BomberScoreValue;
    m_Health = GameData::m_BomberHealth;
    m_MovementSpeed = GameData::m_BomberSpeed;
    SetBoundBox(4,2);
    SetMovementVector(-1,0);
    InitWeapon();
}

void Bomber::InitWeapon(){
    m_Weapon = new RocketLauncher(m_X,m_Y);
    m_Weapon->SetOffsetXY(-1,1);
    m_Weapon->SetDirection(-1);
    m_Weapon->SetLevel(2);
    m_Weapon->SetFireRate(GameData::m_BomberFireRate);
}

Drone::Drone(const int & x,const int & y):Enemy(x,y) {
   SetupStats();
}

void Drone::SetupStats(){
    SetSprite("<X<\n"
		      " `\"\n");
    m_ScoreValue = GameData::m_DroneScoreValue;
    m_Health = GameData::m_DroneHealth;
    m_MovementSpeed = GameData::m_DroneSpeed;
    SetBoundBox(2,2);
    SetMovementVector(-1,1 - 2 * (rand() % 2));    
    InitWeapon();
}

void Drone::InitWeapon(){
    m_Weapon = new Cannon(m_X,m_Y);
    m_Weapon->SetOffsetXY(-1,0);
    m_Weapon->SetDirection(-1);
    m_Weapon->SetLevel(1);
    m_Weapon->SetFireRate(GameData::m_DroneFireRate);
}

void Drone::OnBorderCollision(){
    if (m_Y == GameData::m_TopBorderHeight ){
        m_Y++;
        m_VectY = 1;
    }
    if( m_Y == GameData::m_GameWindowHeight){
        m_Y--;
        m_VectY = -1; 
    }
}

Cruiser::Cruiser(const int & x,const int & y):Enemy(x,y) {
    SetupStats();
}

void Cruiser::Update(){
    ObjectMove();
    std::chrono::system_clock::duration deltaTime = std::chrono::system_clock::now() - m_Clock;
    int deltaMillis = std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime).count();
    //switch to firing state
    if( m_StateSwitch && deltaMillis > GameData::m_CruiserTravelTime){
        m_Clock = std::chrono::system_clock::now();
        m_StateSwitch = false;
        SetMovementVector(0,0);
        return;
    }
    //fire state
    if(!m_StateSwitch && deltaMillis < GameData::m_CruiserFireTime){
        m_Weapon->Update();
    }
    //switch to movement state
    else if(!m_StateSwitch){
        m_Clock = std::chrono::system_clock::now();
        m_StateSwitch = true;
        SetMovementVector(-1,0);
    }
        
}

void Cruiser::SetupStats(){
    SetSprite("   <===\\\n"
		      "   /HHH|{\n"
		      "<########D\n"
		      "   \\HHH|{\n"
		      "   <===/\n");
    m_ScoreValue = GameData::m_CruiserScoreValue;
    m_Health = GameData::m_CruiserHealth;
    m_MovementSpeed = GameData::m_CruiserSpeed;
    SetBoundBox(10,4);
    SetMovementVector(-1,0);    
    InitWeapon();
    m_StateSwitch = true;
    m_Clock = std::chrono::system_clock::now();

}

void Cruiser::InitWeapon(){
    m_Weapon = new MachineGun(m_X,m_Y);
    m_Weapon->SetOffsetXY(-1,2);
    m_Weapon->SetDirection(-1);
    m_Weapon->SetLevel(3);
    m_Weapon->SetFireRate(GameData::m_CruiserFireRate);
}
