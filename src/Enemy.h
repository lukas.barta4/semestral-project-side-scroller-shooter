#ifndef Enemy_H
#define Enemy_H



#include "Entity.h"

class CollisionVisitor;


/**
 * @brief Represents enemy, it shoots diferent type of projectiles and has chance to drop bonus on death
 * 
 */
class Enemy : public Entity{
public:
    /**
     * @brief Construct a new Enemy object for given coordinates
     * 
     * @param x int X coordinate
     * @param y int Y coordinate
     */
    Enemy(const int & x,const int & y);

    /**
     * @brief Overrides Damage function to try spawn bonus and add score to GameScene
     * 
     */
    void Damage(const int & dmg)override;

    /**
     * @brief Has chance to spawn bonus
     * 
     */
    void BonusRoll ();

    /**
     * @brief Overrides update to also update weapon
     * 
     */
    void Update()override;
    void Visit (CollisionVisitor * visitor) override;
    void CollideWith (GameObject * collisionObject) override;

protected:
    int m_ScoreValue;
};

/**
 * @brief Basic enemy with machinegun
 * 
 */
class Fighter : public Enemy{
public:
    Fighter(const int & x,const int & y);
protected:
    void SetupStats()override;
    void InitWeapon()override;
};

/**
 * @brief Basic enemy with rocker launcher
 * 
 */
class Bomber : public Enemy{
public:
    Bomber(const int & x,const int & y);
protected:
    void SetupStats()override;
    void InitWeapon()override;
};

/**
 * @brief Fast low health enemy, bounces off borders
 * 
 */
class Drone : public Enemy{
public:
    Drone(const int & x,const int & y);
protected:
    void SetupStats()override;
    void InitWeapon()override;

    /**
     * @brief Overrides function to bounce off borders
     * 
     */
    void OnBorderCollision() override;
};

/**
 * @brief High health high fire rate enemy, cannot move while firing has cooldown on fire
 * 
 */
class Cruiser : public Enemy{
public:
    Cruiser(const int & x,const int & y);
    /**
     * @brief Overrides function to change states (move/fire) depending on m_clock
     * 
     */
    void Update()override;
protected:
    void SetupStats()override;
    void InitWeapon()override;
    std::chrono::time_point<std::chrono::system_clock> m_Clock; /**< Clock for fire cooldown */
    bool m_StateSwitch;
};




#endif /* Enemy_H */