#include "Entity.h"
#include "Weapon.h"
Entity::~Entity(){
    delete m_Weapon;
    m_Weapon = nullptr;
}



void Entity::SetHealth(const int & health) {
    m_Health = health;
}

int Entity::GetHealth() {
    return m_Health;
}
