#ifndef Entity_H
#define Entity_H
#include "GameObject.h"
class Weapon;

/**
 * @brief Abstract class representing GameObjects that have weapon and health
 * 
 */
class Entity : public GameObject{
    public:
    /**
     * @brief Destroy the Entity object and deletes allocated weapon
     * 
     */
    ~Entity();

    /**
     * @brief Deals damage to self
     * 
     */
    virtual void Damage(const int & dmg) = 0;
    void SetHealth(const int & health);
    int GetHealth();
    
    protected:
    /**
     * @brief Pure virtual function for setring up variables
     * 
     */
    virtual void SetupStats() = 0;

    /**
     * @brief Pure virtual function for initializing weapon
     * 
     */
    virtual void InitWeapon() = 0;
    int m_Health;
    Weapon * m_Weapon;
};

#endif /* Entity_H */