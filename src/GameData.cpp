#include "GameData.h"

    const std::string GameData::m_MapsPath = "saves/levels";
    const std::string GameData::m_ScorePath = "saves/score";
	
    const int GameData::m_TopBorderHeight = 1;
    const int GameData::m_GameWindowHeight = 40;
    const int GameData::m_GameWindowWidth = 202;
    const int GameData::m_HUDHeight = 5;
    const int GameData::m_FPS = 30;
    const int GameData::m_EndAfterLastEnemy = 40000;
    const int GameData::m_TopScore = 5;
    const int GameData::m_ScoreForFinishing = 500;
    const int GameData::m_ScorePerHealth = 10;

    const int GameData::m_PlayerMaxHealth = 9;
    const int GameData::m_PlayerStartHealth = 5;
    const int GameData::m_PlayerInvincibilityDuration = 500;
    const int GameData::m_MoveUpKey = 119;
    const int GameData::m_MoveDownKey = 115;
    const int GameData::m_ExitKey = 120;

    const int GameData::m_BonusChance = 60;
    const int GameData::m_BonusSpeed = 7;

    const int GameData::m_MaxWeaponLevel = 3;

    const int GameData::m_MachineGunFireRate = 110;
    const int GameData::m_BulletSpeed = 67;
    const int GameData::m_BulletDmg = 3;

    const int GameData::m_CannonFireRate = 60;
    const int GameData::m_ShellSpeed = 75;
    const int GameData::m_ShellDmg = 8;

    const int GameData::m_RocketLauncherFireRate = 70;
    const int GameData::m_RocketSpeed = 69;
    const int GameData::m_RocketDmg = 5;
    const int GameData::m_MiniRocketDmg = 3;
    const int GameData::m_MiniRocketSpeed = 50;

    const int GameData::m_FighterScoreValue = 50;
    const int GameData::m_FighterHealth = 15;
    const int GameData::m_FighterFireRate = 20;
    const int GameData::m_FighterSpeed = 8;

    const int GameData::m_BomberScoreValue = 55;
    const int GameData::m_BomberHealth = 25;
    const int GameData::m_BomberFireRate = 25;
    const int GameData::m_BomberSpeed = 7;
    
    const int GameData::m_DroneScoreValue = 10;
    const int GameData::m_DroneHealth = 9;
    const int GameData::m_DroneFireRate = 10;
    const int GameData::m_DroneSpeed = 10;

    const int GameData::m_CruiserScoreValue = 300;
    const int GameData::m_CruiserHealth = 45;
    const int GameData::m_CruiserSpeed = 8;
    const int GameData::m_CruiserFireRate = 500;
    const int GameData::m_CruiserTravelTime = 4500;
    const int GameData::m_CruiserFireTime = 1000;


