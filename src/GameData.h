#ifndef GameData_H
#define GameData_H
#include<string>
/**
 * @brief Constains all constants
 * 
 */
namespace GameData{
    extern const std::string m_MapsPath; /**< Folder location of maps */
    extern const std::string m_ScorePath; /**< File location of scores file */

    extern const int m_TopBorderHeight;
    extern const int m_GameWindowHeight; /**< Height of gaming board */
    extern const int m_GameWindowWidth; /**< Width of gaming board */
    extern const int m_HUDHeight;
    extern const int m_FPS;
    extern const int m_EndAfterLastEnemy; /**< Time it takes to game being over after last enemy is spawned in ms */
    extern const int m_TopScore; /**< Number of top scores being saved */
    extern const int m_ScoreForFinishing;
    extern const int m_ScorePerHealth;

    extern const int m_PlayerMaxHealth;
    extern const int m_PlayerStartHealth;
    extern const int m_PlayerInvincibilityDuration; /**< Time duration in ms */
    extern const int m_MoveUpKey; /**< Key binding for moving up */
    extern const int m_MoveDownKey; /**< Key binding for moving down */
    extern const int m_ExitKey; /**< Key binding for exiting game */

    extern const int m_BonusChance; /**< Chance in % that enemy upon death spawns bonus */
    extern const int m_BonusSpeed; /**< Movement speed in blocks per second */

    extern const int m_MaxWeaponLevel;

    extern const int m_MachineGunFireRate; /**< FireRate in projectiles per second */
    extern const int m_BulletSpeed; /**< Movement speed in blocks per second */
    extern const int m_BulletDmg;

    extern const int m_CannonFireRate; /**< FireRate in projectiles per second */
    extern const int m_ShellSpeed; /**< Movement speed in blocks per second */
    extern const int m_ShellDmg;

    extern const int m_RocketLauncherFireRate; /**< FireRate in projectiles per second */
    extern const int m_RocketSpeed; /**< Movement speed in blocks per second */
    extern const int m_RocketDmg;
    extern const int m_MiniRocketDmg;
    extern const int m_MiniRocketSpeed; /**< Movement speed in blocks per second */

    extern const int m_FighterScoreValue;
    extern const int m_FighterHealth;
    extern const int m_FighterFireRate; /**< FireRate in projectiles per second */ 
    extern const int m_FighterSpeed; /**< Movement speed in blocks per second */
    

    extern const int m_BomberScoreValue;
    extern const int m_BomberHealth;
    extern const int m_BomberFireRate; /**< FireRate in projectiles per second */
    extern const int m_BomberSpeed; /**< Movement speed in blocks per second */

    extern const int m_DroneScoreValue;
    extern const int m_DroneHealth;
    extern const int m_DroneFireRate; /**< FireRate in projectiles per second */
    extern const int m_DroneSpeed; /**< Movement speed in blocks per second */

    extern const int m_CruiserScoreValue;
    extern const int m_CruiserHealth;
    extern const int m_CruiserSpeed;
    extern const int m_CruiserFireRate; /**< FireRate in projectiles per second */
    extern const int m_CruiserTravelTime; /**< Duration that crusier is moving in ms */
    extern const int m_CruiserFireTime; /**< Duration that crusier is firing in ms */

}


#endif /* GameData_H */