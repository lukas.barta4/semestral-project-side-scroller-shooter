#include "GameObject.h"
#include "GameData.h"
#include <math.h>
#include "GameScene.h"
#include "ncurses.h"

GameObject::GameObject(){
    m_LastMovementAt = std::chrono::system_clock::now();
    m_DiagonalCounter= 0;
}



void GameObject::SetBoundBox(int Width,int Height) {
    m_BoundBoxWidth  = Width;
    m_BoundBoxHeight = Height;
}

int GameObject::GetBoundWidth(){
    return m_BoundBoxWidth;
}

int GameObject::GetBoundHeight(){
    return m_BoundBoxHeight;
}

void GameObject::SetMovementVector(int vx, int vy){
    m_VectX = vx;
    m_VectY = vy;
}

int GameObject::GetMoveDirHorizontal(){
    return m_VectX;
}

void GameObject::ObjectMove() {
    // Calculates time difference between last movement and determines move scalar
    std::chrono::system_clock::duration deltaTime = std::chrono::system_clock::now() - m_LastMovementAt;
    int deltaMillis = std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime).count();
    double speedInMillis = m_MovementSpeed / (double) 1000;
    double moveScalar = speedInMillis*deltaMillis;
    if (moveScalar >= 1){
        Clear();
        //Steps for each scalar block and checks for collisions
        for(int i = 1;i<=(int)moveScalar;i++){
            m_X +=(int)m_VectX;
            //diagonal movement
            if(m_DiagonalCounter % 4 == 0){
                m_Y +=(int)m_VectY;
                m_DiagonalCounter = 0;
            }
            m_DiagonalCounter++;
            OnBorderCollision();
            GameScene::GetInstance().ResolveCollisions(this);
            if(isMarked()){
                return;
            }
        }
        if(m_X < -15 || m_X > GameData::m_GameWindowWidth+40){
            Destroy();
            return;
        }
        //Updates clock for delta time
        m_LastMovementAt = std::chrono::system_clock::now();
    }
}

void GameObject::Intersects(GameObject * obj){
    int x1 = obj->GetX();
    int y1 = obj->GetY();
    int x2 = obj->GetX() + obj->GetBoundWidth();
    int y2 = obj->GetY() + obj->GetBoundHeight();
    // Checks if one of the collision boxes is on right/left
    if(m_X > x2 || x1 > m_X + m_BoundBoxWidth){
        return;
    }
    // Checks if one of the collision boxes is under/above
    if(m_Y > y2 || y1 > m_Y + m_BoundBoxHeight){
        return;
    }
    
    CollideWith(obj);
    ForcedRender();
    obj->ForcedRender();
}

void GameObject::OnBorderCollision() {
    if (m_Y == GameData::m_TopBorderHeight){
        m_Y++;
        Destroy();
    }
    if( m_Y == GameData::m_GameWindowHeight){
        m_Y--;
        Destroy();
    }
}
