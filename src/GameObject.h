#ifndef GameObject_H
#define GameObject_H

#include "Object.h"
#include <chrono>

class CollisionVisitor;


/**
 * @brief Abstract class that represents GameObject with collision and movement properties
 * 
 */
class GameObject : public Object {
public:
    /**
     * @brief Construct a new GameObject and initializes internal variables
     * 
     */
    GameObject();

    /**
     * @brief Virtual destructor that ensures correct memory handling in derived classes
     * 
     */
    virtual ~GameObject(){}

    /**
     * @brief Sets the bound box for collision handling
     * 
     * Bound box size is measured from center of character (block)
     * That means that GameObject with sprite represented only by one character will have bound box with width and height 0 
     *
     */
    void SetBoundBox(int Width,int Height);

    int GetBoundWidth();

    int GetBoundHeight();

    /**
     * @brief Sets a movement vector
     * 
     * Movement vector can be either 1,0,-1 for both directions
     *
     */
    void SetMovementVector(int vx, int vy);

    int GetMoveDirHorizontal();

    /**
     * @brief Moves GameObject in direction of a movement vector
     * 
     * Movement is time fixed so performance wont affect movement speed
     *
     */
    void ObjectMove();

    /**
     * @brief Whenever GameObject intersects with obj it resolves collision with use of double dispatch visitor pattern represented by class CollisionVisitor
     *
     */
    void Intersects(GameObject * obj);

    /**
     * @brief Pure virtual function for accepting visitor
     * 
     * Second dispatch
     * 
     * @param visitor Visitor class object for resolving collision
     */
    virtual void Visit (CollisionVisitor * visitor) = 0;

    /**
     * @brief Creates Visitor object and passes collisionObject parameter
     * 
     * @param collisionObject 
     */
    virtual void CollideWith (GameObject * collisionObject) = 0;
protected:
    /**
     * @brief Destroys GameObject if it collides with top and bottom border
     * 
     */
    virtual void OnBorderCollision();
    int m_BoundBoxWidth;
    int m_BoundBoxHeight;
    int m_MovementSpeed; /**< Block per second */ 
    std::chrono::time_point<std::chrono::system_clock> m_LastMovementAt; /**< Clock for calculating delta movement */
    int m_VectX, m_VectY;
    int m_DiagonalCounter; /**< Makes Y step skipping */
};
#endif /* GameObject_H */