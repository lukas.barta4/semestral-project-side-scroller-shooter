#include "GameScene.h"
#include "GameData.h"
#include "ncurses.h"
#include <thread>
#include <chrono>
#include "GameObject.h"
#include "LevelManager.h"
#include "Projectile.h"
#include "Player.h"
#include "Enemy.h"
#include "Bonus.h"


GameScene& GameScene::GetInstance(){
    static GameScene instance;
    return instance;
}

GameScene::~GameScene(){
    DeleteAll();
    CollectGarbage();
    m_Player = nullptr;
}


void GameScene::Start(){
    InitNCurses();
    InitScene();
    //starts timer for spawning enemies
    LevelManager::GetInstance().Start();
    Loop();
    clear();
    refresh();
    endwin();
    
}

void GameScene::EndGame(){
    m_Loop = false;
    //adds score bonus for finishing map
    if(LevelManager::GetInstance().CheckEndOfMap()){
        m_Score += GameData::m_ScoreForFinishing;
    }
    //adds score bonus for health
    m_Score +=m_Player->GetHealth()*GameData::m_ScorePerHealth; 
    PrintGameOver();
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	flushinp();
}



void GameScene::InitNCurses(){
    initscr();
    noecho();
    curs_set(0);
    nodelay(stdscr, TRUE);
    keypad(stdscr, TRUE);
}

void GameScene::InitScene(){
    m_Score = 0;
    m_Loop = true;
    m_Player = new Player();
    m_Objects.push_back(m_Player);
    m_Objects.push_back(new MachineGunBonus(200,20));
    srand (time(NULL));
    DrawHUD();
}

void GameScene::Loop() {
    //delta time fixed loop
    while (m_Loop){
        std::chrono::time_point start = std::chrono::system_clock::now() + std::chrono::milliseconds(1000/GameData::m_FPS);
        LevelManager::GetInstance().Update();
        HandleKey();
        UpdateObjects();
        CollectGarbage();
        RenderScene();
        std::this_thread::sleep_until(start);
    }
    DeleteAll();
}




void GameScene::UpdateObjects() {
    std::list<GameObject *>::iterator it = m_Objects.begin();
    while(it!=m_Objects.end() && m_Loop ){
        (**it).Update();
        it++;
    }
}

void GameScene::ResolveCollisions(GameObject * obj){
    std::list<GameObject *>::iterator it = m_Objects.begin();
    while(it!=m_Objects.end() && m_Loop ){
        if(obj!=*it && !(**it).isMarked()){
            obj->Intersects(*it);
        }
        if(obj->isMarked()){
            return;
        }
        it++;
    }
}

void GameScene::CollectGarbage() {
    std::list<GameObject *>::iterator itObj = m_Objects.begin();
    while (itObj!=m_Objects.end()){
        if ((**itObj).isMarked()){
            delete *itObj;
            itObj = m_Objects.erase(itObj);
        }
        else{
            itObj++;
        }
    }
    
}

void GameScene::RenderScene() {
    if(!m_Loop){
        return;
    }
    std::list<GameObject *>::iterator it;
    for (it = m_Objects.begin(); it!=m_Objects.end(); it++){
        (**it).Render();
        refresh();
    }
    
}


void GameScene::DeleteAll(){
    std::list<GameObject *>::iterator itObj = m_Objects.begin();
    while (itObj!=m_Objects.end()){
        delete *itObj;
        itObj = m_Objects.erase(itObj);
    }
    
}

void GameScene::AddObject(GameObject * object){
    m_Objects.push_front(object);
}



void GameScene::AddScore(const int & score){
    m_Score += score;
    DrawHUD();
}

int GameScene::GetScore()const{
    return m_Score;
}

void GameScene::HandleKey(){
    int key = getch();
    if(key == GameData::m_ExitKey){
        EndGame();
        return;
    }
    //on resize event reloads ncurses stdscr
    else if(key == KEY_RESIZE){
        endwin();
        clear();
        InitNCurses();
        DrawHUD();
        m_Player->ForcedRender();
        refresh();
    }
    m_Player->KeyPressed(key);
}

void GameScene::DrawHUD(){
    int x,y;
    getmaxyx(stdscr,y,x);
    move(GameData::m_TopBorderHeight,0);
    hline(' ' | A_REVERSE,x);
    if (GameData::m_GameWindowHeight + 1 <= y){
        move(GameData::m_GameWindowHeight + 1,0);
        hline(' '|A_REVERSE,x);
    }
    if(GameData::m_GameWindowHeight + 2 <= y){
        move(GameData::m_GameWindowHeight + 2,0);
        addch(' ' | A_REVERSE);
        addch(' ' | A_REVERSE);
        printw("Health: %d ", m_Player->GetHealth());
        move(GameData::m_GameWindowHeight + 2,20);
        printw("Weapon level: %d", m_Player->GetWeaponLevel());
        move(GameData::m_GameWindowHeight + 2,40);
        printw("Score: %d", m_Score);
        move(GameData::m_GameWindowHeight + 2,x-2);
        addch(' ' | A_REVERSE);
        addch(' ' | A_REVERSE);
    }
    if(GameData::m_GameWindowHeight + 3 <= y){
        move(GameData::m_GameWindowHeight + 3,0);
        hline(' ' | A_REVERSE,x);
    }
}

void GameScene::PrintGameOver(){
    clear();
    move (GameData::m_GameWindowHeight/2-1,GameData::m_GameWindowWidth/2 - 4);
    printw("GameOver");
    move (GameData::m_GameWindowHeight/2+1,GameData::m_GameWindowWidth/2 - 4);
    printw("Score:  %d",m_Score);
    refresh();
}