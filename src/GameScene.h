#ifndef GameScene_H
#define GameScene_H

#include <list>

class GameObject;
class Player;
class Projectile;
class Enemy;


class Player;
class Enemy;
class Bonus;

/**
 * @brief Mayer singlton class of GameScene, keeps track of all GameObjects. Updates GameObjects, resolves collisions using CollisionVisitor
 *
 */
class GameScene{
public:
/**
 * @brief Get the Instance object
 * 
 */
static GameScene& GetInstance();

/**
 * @brief Destroy the Game Scene object and delete all alocated resources
 * 
 */
~GameScene();

/**
 * @brief Starts game and spawns player
 * 
 */
void Start();

/**
 * @brief Adds GameObject to list of GameObjects
 * 
 */
void AddObject(GameObject * obj);

/**
 * @brief Iterates over all GameObjects in memory and performs intersection
 * 
 */
void ResolveCollisions(GameObject * obj);

/**
 * @brief Ends game displays score
 * 
 */
void EndGame();

/**
 * @brief Draws overlay, health, weapon level,score
 * 
 */
void DrawHUD();

void AddScore(const int & score);
int GetScore()const;

private:
/**
 * @brief Private constructor to achiave singleton pattern
 * 
 */
GameScene(){}

 /**
  * @brief Game loop updates, renders, resolves, collision, spawn GameObjects
  * 
  */
void Loop();

/**
 * @brief Collects and deletes flaged GameObjects
 * 
 */
void CollectGarbage();

/**
 * @brief Iterates over GameObjects and renders them
 * 
 */
void RenderScene();

/**
 * @brief Iterates iver GameObjets and updates them
 * 
 */
void UpdateObjects();

/**
 * @brief Deletes all GameObjects
 * 
 */
void DeleteAll();

/**
 * @brief Procceses key input
 * 
 */
void HandleKey();

/**
 * @brief Initialize scene at start of new game
 * 
 */
void InitScene();

/**
 * @brief Initilize ncurses library
 * 
 */
void InitNCurses();

/**
 * @brief Prints game over screen 
 * 
 */
void PrintGameOver();

std::list<GameObject *> m_Objects;

Player * m_Player;
bool m_Loop; /**< Game loop flag*/
int m_Score;
};

#endif /* GameScene_H */