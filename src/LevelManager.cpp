#include "LevelManager.h"

#include <iostream>
#include <string>
#include <fstream>
#include "SpawnData.h"
#include "GameData.h"
#include "GameScene.h"
#include "Enemy.h"

LevelManager& LevelManager::GetInstance() {
    static LevelManager instance;
    return instance;
}

LevelManager::~LevelManager(){
    ClearData();
}

void LevelManager::Start() {
    m_EndOfMap = false;
    m_Clock = std::chrono::system_clock::now();
}


void LevelManager::Update() {
    if(CheckEndOfMap()){
        //Level is finished
        GameScene::GetInstance().EndGame(); 
        return;
    }
    std::chrono::system_clock::duration deltaTime = std::chrono::system_clock::now() - m_Clock;
    int deltaMillis = std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime).count();
    if(!m_ObjectToSpawn.empty() && m_ObjectToSpawn.front()->m_SpawnTime <= deltaMillis){
        GameScene::GetInstance().AddObject(PopObject());
    }
}

bool LevelManager::CheckEndOfMap(){
    //Starts timer to end game
    if(!m_EndOfMap && m_ObjectToSpawn.empty()){
        m_EndOfMap = true;
        m_Clock = std::chrono::system_clock::now();
        return false;
    }
    std::chrono::system_clock::duration deltaTime = std::chrono::system_clock::now() - m_Clock;
    int deltaMillis = std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime).count();
    if(m_EndOfMap && deltaMillis >= GameData::m_EndAfterLastEnemy){
        return true;
    }
    return false;
}

bool LevelManager::LoadLevel(std::string fname){
    ClearData();
    std::string line;
    std::ifstream f(GameData::m_MapsPath + "/" + fname,std::fstream::in);
    int prevtime = 0;
    int time,pos,type;
    int lineCounter = 1;
    while(getline(f, line)) {
        //Parse and check for syntax error
        if(!ParseLine(line,time,pos,type) || prevtime > time || type >= m_TypesEnemies){
            std::cout<<"\n  --Invalid line: " << lineCounter << "\n";
            f.close();
            return false;
        }
        //creates spawn data block according to level file
        SpawnData * DataBlock = new SpawnData();
        DataBlock->m_SpawnTime = time;
        DataBlock->m_SpawnYPos = pos;
        DataBlock->m_Type = type;
        m_ObjectToSpawn.push_back(DataBlock);
        prevtime = time;
        lineCounter++;
    }
    f.close();
    return true;
}

bool LevelManager::ParseLine(const std::string & input, int & time, int & pos, int & type){
    std::string::const_iterator it = input.begin();
    std::string::const_iterator end = input.end()--;
    //parses line
    if(it==end || !ParseColumn(it,end,time) || !ParseColumn(it,end,pos) || !ParseColumn(it,end,type)){
        return false;
    }
    return true;
}

bool LevelManager::ParseColumn(std::string::const_iterator & it, std::string::const_iterator end, int & output){
    std::string parsed;
    for (;it!= end;it++){
    	if(isdigit((int)*it)){
    		parsed.push_back(*it);
    	}
    	else if(*it == ',' || *it == '\r'){
    		it++;
    		break;
    	}
    	else{
    		return false;
    	}
    }
    //converts parsed string to int
    output = std::stoi(parsed);
    return true;
}

void LevelManager::ClearData(){
    std::list<SpawnData *>::iterator itObj = m_ObjectToSpawn.begin();
    while (itObj!=m_ObjectToSpawn.end()){
        delete *itObj;
        itObj = m_ObjectToSpawn.erase(itObj);
    }
}

Enemy * LevelManager::PopObject(){
    int type = m_ObjectToSpawn.front()->m_Type;
    int posY = m_ObjectToSpawn.front()->m_SpawnYPos;
    Enemy * enemy;
    delete m_ObjectToSpawn.front();
    m_ObjectToSpawn.pop_front();
    switch (type)
    {
    case 0:
        enemy = new Fighter(GameData::m_GameWindowWidth,posY);
        break;
    case 1:
        enemy = new Bomber(GameData::m_GameWindowWidth,posY);
        break;
    case 2:
        enemy = new Drone(GameData::m_GameWindowWidth,posY);
        break;
    default:
        enemy = new Cruiser(GameData::m_GameWindowWidth,posY);
        break;
    }
    //checks if Y position of created enemy is correct if not Enemy is relocated into game window boundaries
    if(posY <= GameData::m_TopBorderHeight){
        enemy->SetXY(GameData::m_GameWindowWidth,GameData::m_TopBorderHeight +1);
    }
    if(posY + enemy->GetBoundHeight() >= GameData::m_GameWindowHeight){
        enemy->SetXY(GameData::m_GameWindowWidth,GameData::m_GameWindowHeight -1 -enemy->GetBoundHeight());
    }
    return enemy;
}