#ifndef LevelManager_H
#define LevelManager_H



#include <chrono>
#include <string>
#include <list>

class SpawnData;
class Enemy;
/**
 * @brief Mayer singlton class of LevelMagager which manages loading of level and spawning enemies
 * 
 */
class LevelManager {
public:
    /**
     * @brief Get the Instance object
     * 
     */
    static LevelManager& GetInstance();
    /**
     * @brief Destroy the Level Manager object
     * 
     */
    ~LevelManager();

    /**
     * @brief Start spawining enimies
     * 
     */
    void Start();

    /**
     * @brief Update messege from game loop spawns enemies if duration for spawning enemy has passed 
     * 
     */
    void Update();

    /**
     * @brief Loads level into a buffer
     * 
     * @param fname name of level file
     * @return true if file loaded correctly
     * @return false if file has incorrect syntax
     */
    bool LoadLevel(std::string fname);

    /**
     * @brief Checks whenever level is finished
     * 
     */
    bool CheckEndOfMap();
private:

    /**
    * @brief Private constructor to achiave singleton pattern
    * 
    */
    LevelManager(){}

    /**
     * @brief Parses line input into output parameters
     * 
     * @param input string to parse by function
     * @param time output parameter for time
     * @param pos output parameter for position on Y axis
     * @param type output parameter for type of enemy to spawn
     * @return true if input string has correct format
     * @return false if input string has incorrect format
     */
    bool ParseLine(const std::string & input, int & time, int & pos, int & type);

    /**
     * @brief Parses collumn until field separator is met and saves it to the ouput parameter
     * 
     * @param it string const iterator from where function will start parsing
     * @param end string const interator bound to which function will try to parse
     * @param output output parameter int
     * @return true if input string has correct format
     * @return false if input string has correct format
     */
    bool ParseColumn(std::string::const_iterator & it, std::string::const_iterator end, int & output);

    /**
     * @brief Clears buffer
     * 
     */
    void ClearData();
    
    /**
     * @brief Pop Spawndata object from buffer and creates corresponding enemy
     * 
     * @return Enemy* created enemy from map
     */
    Enemy * PopObject();
    std::chrono::time_point<std::chrono::system_clock> m_Clock; /**< Timer for screating enemies*/
    std::list<SpawnData *> m_ObjectToSpawn; /**< Data buffer*/
    bool m_EndOfMap; /**< End of map state flag*/
    const int m_TypesEnemies = 4; 

};
#endif /* LevelManager_H */