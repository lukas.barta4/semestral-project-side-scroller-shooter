#include "Menu.h"
#include "GameData.h"
#include "GameScene.h"
#include "LevelManager.h"
#include "ScoreManager.h"
#include <ncurses.h>
#include <iostream>
#include <string>
#include <filesystem>


void Menu::StartMenu() {
    PrintIntro();
    std::string input;
    std::string param;
    std::getline(std::cin, input);
    //input loop
    while (input != "exit") {         
        if (input == "help"){
            PrintHelp();
        }
        else if (input == "start"){
            std::cout<<"    To start select one level\n"
                       "    Available levels:\n";
            
			PrintLevelNames();
            std::getline(std::cin, input);
            //checks file and loads level
            if(CheckLevelName(input) && LoadLevel(input)){
                GameScene::GetInstance().Start();
                std::cout<<"\n    --Type nick name for saving score--\n";
                std::getline(std::cin, input);
                if(input != ""){
                    ScoreManager::GetInstance().SaveScore(input,GameScene::GetInstance().GetScore());
                    std::cout<< "\n    --Score saved--\n\n";
                }
            }
        }        
        else if (input == "tutorial"){
            PrintTutorial();
        }        
        else if (input == "score screen"){
            ScoreManager::GetInstance().PrintScore();
        }        
        else if (input == "check terminal"){
            CheckTerminal();
        }        
        else if (input != ""){
            std::cout <<"\n  --Error: wrong command--\n";
            std::cout << "  for info type \"help\"\n\n";
        }
		std::cin.clear();
        std::getline(std::cin, input);    
    }
}

void Menu::CheckTerminal(){
    int x,y;
    initscr();
    refresh();
    getmaxyx(stdscr,y,x);
    endwin();

    if (y < ( GameData::m_GameWindowHeight+GameData::m_HUDHeight) || x < GameData::m_GameWindowWidth ){
        std::cout<< "\n   Size of terminal is not sufficient to render whole game.\n"
                    "   Parts of screen could be missing\n"
                    "   Current width terminal is " << x << ", and height is " << y << ".\n"
                    "   Resizing your terminal window is highly recommended\n"
                    "   Optimal width is " <<  GameData::m_GameWindowWidth << " and height "<<GameData::m_GameWindowHeight+GameData::m_HUDHeight<< "\n\n";
    }
    else{
        std::cout<< "\n   Size of terminal is sufficient\n\n";
    }
}

bool Menu::CheckLevelName(std::string fname)const{
    if(!std::filesystem::exists(GameData::m_MapsPath)){
        std::cout<<"\n    --Error: Maps directory doesnt exist--\n";
        return false;
    }
    for (const std::filesystem::directory_entry & entry : std::filesystem::directory_iterator(GameData::m_MapsPath)){
		if (entry.is_regular_file() && fname == entry.path().filename()){
            return true;
        }
	}
    std::cout << "\n    --Error: Wrong level name--\n\n";
	return false;
}

void Menu::PrintLevelNames()const{
	std::string fname;
    if(!std::filesystem::exists(GameData::m_MapsPath)){
        std::cout<<"\n    --Error: Maps directory doesnt exist--\n";
        return;
    }
    for (const std::filesystem::directory_entry & entry : std::filesystem::directory_iterator(GameData::m_MapsPath)){
        if (entry.is_regular_file()) {
		    fname = entry.path().filename();
            std::cout << "      "<< fname << std::endl;
        }
    }
}

bool Menu::LoadLevel(std::string fname){
    if(LevelManager::GetInstance().LoadLevel(fname)){
        return true;
    }
    std::cout << "\n  --Error: Level file corrupted--\n\n";
    return false;
	
}

void Menu::PrintIntro()const{
    std::cout << "   \\\\*****************************//\n";
    std::cout << "   ||Welcome to Space fighter 2020||\n";
    std::cout << "   //*****************************\\\\\n\n";
    std::cout << "   for info type \"help\"\n\n";
}

void Menu::PrintHelp()const{
    std::cout<< "\n       --commands--\n\n";
    std::cout<< "   start => starts game\n\n";
    std::cout<< "   tutorial => basic explenation of game\n\n";
    std::cout<< "   score screen => shows top ten local all time scores\n\n";
    std::cout<< "   check terminal => checks if size of terminal is sufficient\n\n";
    std::cout<< "   exit => closes the game\n\n";
}

void Menu::PrintTutorial()const{
    std::cout<< "\n       --Tutorial--\n\n";
    std::cout<< "   Goal of game is to reach end of map and destroy as much enemy ships as possible with your ship.\n";
    std::cout<< "   Your ship has " << GameData::m_PlayerStartHealth<< " at the start, and maximum health of your ship is " << GameData::m_PlayerMaxHealth<< ".\n";
    std::cout<< "   If your ship collides with either enemy projectile or enemy ships your ship takes one damage.\n";
    std::cout<< "   Upon receiving damage your ship is invulnerable for "<<GameData::m_PlayerInvincibilityDuration/1000.0 <<" seconds.\n";
    std::cout<< "   Once ship health reaches zero game ends.\n";
    std::cout<< "   Ship fires automatically and movement is controlled with keys: "<< (char)GameData::m_MoveUpKey<< "/" << (char)GameData::m_MoveDownKey<< " for up/down movement.\n";
    std::cout<< "   Game can be exited by pressing key: "<< (char)GameData::m_ExitKey<<".\n";
    std::cout<< "   Ship has available 3 types of weapons.\n";
    std::cout<< "   MachineGun (M) - Starter weapon. Fast firing low damage weapon. Diagonal bullets bounce off borders.\n";
    std::cout<< "   Cannon (C) - Slow firing hard hitting weapon \n";
    std::cout<< "   Rocker launcher (C) - Balance weapon. Fires only directly forward\n";
    std::cout<< "   Bonuses are represented by corresponding starting cappital letter of said bonus\n";
    std::cout<< "   There are two types of bonuses health (H) and weapon bonuses (C,R,M)\n";
    std::cout<< "   Health bonus regenerates one point of health.\n";
    std::cout<< "   Weapon bonus upgrades corresponding weapon or swaps to a new one with weapon level 1\n";
    std::cout<< "   Score is increased by: killing enemy, finishing map, remaining health.\n\n";
}
    