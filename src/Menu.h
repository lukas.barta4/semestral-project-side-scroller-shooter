#ifndef Menu_H
#define Menu_H

#include<string>
/**
 * @brief Menu class for managing user input
 * 
 */
class Menu{
public:
    void StartMenu();
private:
    /**
     * @brief Check if terminal is sufficient in size
     * 
     */
    void CheckTerminal();
    /**
     * @brief Checks if level name is valid
     * 
     * @param fname string name of level
     * @return true if level name exist and is valid
     * @return false if level doesnt exist or map directory path is invalid
     */
    bool CheckLevelName(std::string fname)const;

    /**
     * @brief Prints name of levels insade map directory
     * 
     */
    void PrintLevelNames()const;

    /**
     * @brief Loads level by level name
     * 
     * @param fname Name of level file
     * @return true if level loads correctly
     * @return false if level file is corrupted
     */
    bool LoadLevel(std::string fname);

    void PrintIntro()const;

    /**
     * @brief Prints commands 
     * 
     */
    void PrintHelp()const;

    /**
     * @brief Prints basic tutorial
     * 
     */
    void PrintTutorial()const;
};



#endif /* Menu_H */