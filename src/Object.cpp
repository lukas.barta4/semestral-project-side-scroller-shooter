#include "Object.h"
#include "GameData.h"

#include <ncurses.h>

Object::Object() {
    m_MarkForDestruction = false;
    m_PositionChanged = true;
}

int Object::GetX() const {
    return m_X;
}

int Object::GetY() const {
    return m_Y;
}


void Object::ForcedRender(){
    m_PositionChanged = true;
}

void Object::Render(){
    if(!m_PositionChanged){
        return;
    }
    m_PositionChanged = false;
    int charPosX = m_X;
    int charPosY = m_Y;
    int termWidth,termHeight;
    bool moveCursor = false;
    getmaxyx(stdscr,termHeight,termWidth);
    //Prints sprite to stdscr
    for (auto it = m_Sprite.begin(); it!=m_Sprite.end();it++){
        if(*it =='\n'){
            charPosX=m_X;
            charPosY++;
            continue;
        }
        //Makes sure that characters is being printed outside terminal bounds
        if (charPosY < termHeight && charPosX < termWidth && 0 <= charPosX){
            if (m_X == charPosX || moveCursor){
                move(charPosY,charPosX);
                moveCursor = false;
            }
            charPosX++;
            addch(*it);
        }
        else{
            charPosX++;
            moveCursor = true;
        }
        
    }
    refresh();
}

void Object::Clear(){
    int charPosX = m_X;
    int charPosY = m_Y;
    int termWidth,termHeight;
    bool moveCursor = false;
    getmaxyx(stdscr,termHeight,termWidth);
    //Clears sprite from stdscr 
    for (auto it = m_Sprite.begin(); it!=m_Sprite.end();it++){
        if(*it =='\n'){
            charPosX=m_X;
            charPosY++;
            continue;
        }
        //Makes sure that character is being printed outside terminal bounds
        if (charPosY < termHeight && charPosX < termWidth && 0 <= charPosX){
            if (m_X == charPosX || moveCursor){
                move(charPosY,charPosX);
                moveCursor = false;
            }
            charPosX++;
            addch(' ');
        }
        else{
            charPosX++;
            moveCursor = true;
        }
    }
    m_PositionChanged = true;
    refresh();
}

void Object::SetSprite(const std::string & spr) {
    m_Sprite = spr;
}

void Object::SetXY(int x,int y) {
    m_X = x;
    m_Y = y;
    m_PositionChanged = true;
}

void Object::Destroy() {
    m_MarkForDestruction = true;
    Clear();
}

bool Object::isMarked() const{
    return m_MarkForDestruction;
}
