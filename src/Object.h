#ifndef Object_H
#define Object_H

#include <string>

/**
 * @brief Abstract class that manages position, rendering of it self and allows for memory managment by GameScene
 * 
 */
class Object{
public:
    /**
     * @brief Construct a new Object and initializes internal variables
     * 
     */
    Object();

    int GetX() const;

    int GetY() const;

    /**
     * @brief Forces Object to render even if position didnt changed
     * 
     */
    void ForcedRender();

    /**
     * @brief Displays Object to ncurses stdscr
     * 
     */
    void Render();
    
    /**
     * @brief Clears sprite that is being displayed to stdscr
     * 
     */
    void Clear();    

    /**
     * @brief Sets sprite for later rendering of Object
     * 
     * @param spr string representing sprite
     */
    void SetSprite(const std::string & spr);

    /**
     * @brief Sets position of Object
     * 
     */
    void SetXY(int x, int y);

    /**
     * @brief Pure virtual Update function that is used in GameScene's game loop
     * 
     */
    virtual void Update() = 0;

    /**
     * @brief Marks Object for destruction and garbage collection by GameScene
     * 
     */
    void Destroy();

    bool isMarked() const;
    
protected:
    bool m_MarkForDestruction; /**< Flag for memory managment */
    bool m_PositionChanged; /**< Flag for positional changes */
    int m_X, m_Y;
    std::string m_Sprite;
};

#endif /* Object_H */