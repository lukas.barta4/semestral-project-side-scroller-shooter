#include "Player.h"
#include "GameData.h"
#include "GameScene.h"
#include "CollisionVisitor.h"
#include "Weapon.h"
Player::Player() {
    SetupStats();
}



void Player::Update() {
    //check if movement is not out of bounds
    if ((m_VectY==-1 && m_Y > GameData::m_TopBorderHeight + 1 )|| (m_VectY == 1 && m_Y < (GameData::m_GameWindowHeight-m_BoundBoxHeight))){
        Clear();
        m_Y +=m_VectY;
        m_VectY=0;
    }
    m_Weapon->Update();
 }


void Player::SetupStats() {
    SetSprite(
        "|\\\n"
        ">@>\n"
        "|/\n");
    SetXY (4,GameData::m_GameWindowHeight/2);
    SetMovementVector(0,0);
    SetBoundBox(2,2);
    m_Clock = std::chrono::system_clock::now();
    m_Health = GameData::m_PlayerStartHealth;
    m_Weapon = new MachineGun(m_X,m_Y);
    InitWeapon();
}
void Player::InitWeapon(){
    m_Weapon->SetLevel(1);
    m_Weapon->SetDirection(1);
    m_Weapon->SetOffsetXY(3,1);
}
void Player::KeyPressed(const int & key){
    if (key==GameData::m_MoveUpKey){
        m_VectY = -1; 
    }
    else if (key==GameData::m_MoveDownKey){
        m_VectY=1;
    }
}

void Player::Damage(const int & dmg){
    std::chrono::system_clock::duration deltaTime = std::chrono::system_clock::now() - m_Clock;
    int deltaMillis = std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime).count();
    //Checks if invicibility duration has been reach
    if(deltaMillis < GameData::m_PlayerInvincibilityDuration){
        return;
    }
    m_Clock = std::chrono::system_clock::now();
    m_Health-=dmg;
    //Updates game HUD
    GameScene::GetInstance().DrawHUD();
    if (m_Health<=0){
        GameScene::GetInstance().EndGame();
    }
    
}

int Player::GetWeaponLevel()const{
    return m_Weapon->GetLevel();
}

void Player::Visit (CollisionVisitor * visitor) {
    visitor->VisitPlayer(this);
}

void Player::CollideWith (GameObject * collisionObject){
    CollisionPlayer visitor{this};
    collisionObject->Visit(&visitor);
}

void Player::Heal(){
    if (m_Health < GameData::m_PlayerMaxHealth){
        m_Health++;
    }
    GameScene::GetInstance().DrawHUD();
}
void Player::UpgradeRocket(){
    if(m_Weapon->GetType()==RocketLauncher::m_Type){
        m_Weapon->Upgrade();
    }
    else{
        delete m_Weapon;
        m_Weapon = new RocketLauncher(m_X, m_Y);
        InitWeapon();
    }
}

void Player::UpgradeMachineGun(){
    if(m_Weapon->GetType()==MachineGun::m_Type){
        m_Weapon->Upgrade();
    }
    else{
        delete m_Weapon;
        m_Weapon = new MachineGun(m_X, m_Y);
        InitWeapon();
    }
}
void Player::UpgradeCanon(){
    if(m_Weapon->GetType()==Cannon::m_Type){
        m_Weapon->Upgrade();
    }
    else{
        delete m_Weapon;
        m_Weapon = new Cannon(m_X, m_Y);
        InitWeapon();
    }
}
