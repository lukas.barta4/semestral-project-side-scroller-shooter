#ifndef Player_H
#define Player_H

//#include "GameObject.h"
#include "Entity.h"

//#include "Bonus.h"
class CollisionVisitor;

/**
 * @brief Represent player, takes user input
 * 
 */
class Player : public Entity{
public:
    Player();
    void Update();
    /**
     * @brief Handles key input for movement
     * 
     * @param key input key int code
     */
    void KeyPressed(const int & key);
    
    /**
     * @brief Deals damage to player and if ends game if health reaches zero. Can be damaged only once per set duration
     * 
     */
    void Damage(const int & dmg)override;

    int GetWeaponLevel()const;
    void Visit (CollisionVisitor * visitor) override;
    void CollideWith (GameObject * collisionObject) override;
    /**
     * @brief Heal bonus modifier
     * 
     */
    void Heal();

    /**
     * @brief Rocket luncher upgrade modifier
     * 
     */
    void UpgradeRocket();

    /**
     * @brief Machine gun upgrade modifier
     * 
     */
    void UpgradeMachineGun();
    
    /**
     * @brief Cannon upgrade modifier
     * 
     */
    void UpgradeCanon();
    
protected:
    void InitWeapon()override;
    void SetupStats()override;
    std::chrono::time_point<std::chrono::system_clock> m_Clock;
};

#endif /* Player_H */