#include "Projectile.h"
#include "GameData.h"
#include "CollisionVisitor.h"

Projectile::Projectile(int x , int y , int vectX, int vectY) {
    if(y == GameData::m_TopBorderHeight || y == GameData::m_GameWindowHeight + 1){
        Destroy();
    }
    SetXY(x,y);
    SetMovementVector(vectX,vectY);
    SetBoundBox(0,0);    
}

void Projectile::Update() {
    ObjectMove();
}

int Projectile::GetDmg()const{
    return m_Damage;
}

void Projectile::Visit (CollisionVisitor * visitor) {
    visitor->VisitProjectile(this);
}
void Projectile::CollideWith (GameObject * collisionObject){
    CollisionProjectile visitor{this};
    collisionObject->Visit(&visitor);
}

Rocket::Rocket(int x , int y , int vectX, int vectY):Projectile(x,y,vectX,vectY) {
    if(vectX == 1){
        SetSprite("=-\n");
    }
    else{
        SetSprite("-=\n");
    }
    SetBoundBox(1,0);
    m_Damage = GameData::m_RocketDmg;
    m_MovementSpeed = GameData::m_RocketSpeed;
}

MiniRocket::MiniRocket(int x , int y , int vectX, int vectY):Projectile(x,y,vectX,vectY) {
    SetSprite("-\n");
    m_Damage = GameData::m_MiniRocketDmg;
    m_MovementSpeed = GameData::m_MiniRocketSpeed;
}


Shell::Shell(int x , int y , int vectX, int vectY):Projectile(x,y,vectX,vectY) {
    SetSprite("0\n");
    m_MovementSpeed = GameData::m_ShellSpeed;
    m_Damage = GameData::m_ShellDmg;
}



Bullet::Bullet(int x , int y , int vectX, int vectY):Projectile(x,y,vectX,vectY) {
    SetSprite("o\n");
    m_MovementSpeed = GameData::m_BulletSpeed;
    m_Damage = GameData::m_BulletDmg;
}

void Bullet::OnBorderCollision(){
    if (m_Y == GameData::m_TopBorderHeight ){
        m_Y++;
        m_DiagonalCounter = 0;
        m_VectY = 1;
    }
    if( m_Y == GameData::m_GameWindowHeight){
        m_Y--;
        m_DiagonalCounter = 0;
        m_VectY = -1; 
    }
}


