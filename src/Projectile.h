#ifndef Projectile_H
#define Projectile_H

#include "GameObject.h"
#include <utility>

class CollisionVisitor;
/**
 * @brief class representing projectiles with their own speed, damage and sprites
 * 
 */
class Projectile : public GameObject{
    public:
    /**
     * @brief Construct a new Projectile object with given coordinates and movement vector
     * 
     * @param x int X coordinate
     * @param y int Y coordinate
     * @param vectX int movement vector X direction
     * @param vectY int movement vector Y direction
     */
    Projectile(int x , int y , int vectX, int vectY);
    void Update();
    void Visit (CollisionVisitor * visitor) override;
    void CollideWith (GameObject * collisionObject) override;
    int GetDmg()const;

protected:
    int m_Damage;
};

/**
 * @brief Rocket projectile balanced big projectile
 * 
 */
class Rocket : public Projectile{
    public:
    /**
     * @brief Construct a new Rocket object with given coordinates and movement vector
     * 
     * @param x int X coordinate
     * @param y int Y coordinate
     * @param vectX int movement vector X direction
     * @param vectY int movement vector Y direction
     */
    Rocket(int x , int y , int vectX, int vectY);
};

/**
 * @brief MiniRocket mini projectile supplementing Rocket projectile
 * 
 */
class MiniRocket : public Projectile {
    public:
    /**
     * @brief Construct a new Mini Rocket object with given coordinates and movement vector
     * 
     * @param x int X coordinate
     * @param y int Y coordinate
     * @param vectX int movement vector X direction
     * @param vectY int movement vector Y direction
     */
    MiniRocket(int x , int y , int vectX, int vectY);
};

/**
 * @brief Cannon projectile fast and hard hitting
 * 
 */
class Shell : public Projectile{
    public:
    /**
     * @brief Construct a new Shell object with given coordinates and movement vector
     * 
     * @param x int X coordinate
     * @param y int Y coordinate
     * @param vectX int movement vector X direction
     * @param vectY int movement vector Y direction
     */
    Shell(int x , int y , int vectX, int vectY);
};

/**
 * @brief Bullet projectile with ability to bounce off borders
 * 
 */
class Bullet : public Projectile{
    public:
    /**
     * @brief Construct a new Bullet object with given coordinates and movement vector
     * 
     * @param x int X coordinate
     * @param y int Y coordinate
     * @param vectX int movement vector X direction
     * @param vectY int movement vector Y direction
     */
    Bullet(int x , int y , int vectX, int vectY);
    
    /**
     * @brief Makes bullets bounce off borders
     * 
     */
    void OnBorderCollision() override;
};

#endif /* Projectile_H */