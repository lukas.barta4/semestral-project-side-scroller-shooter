#include "ScoreManager.h"

#include "GameData.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>


ScoreManager & ScoreManager::GetInstance() {
    static ScoreManager instance;
    return instance;
}

void ScoreManager::PrintScore() {
    LoadScoreFile();
    auto itN = m_Names.begin();
    auto itS = m_Scores.begin();
    int counter = 1;
    while(itN!=m_Names.end()&&itS!=m_Scores.end()){
        std::cout<< "\n   "<<counter<<".  "<<*itN << "  -->  "<<*itS<<"\n";
        counter++;
        itN++;
        itS++;
    }
}

void ScoreManager::SaveScore(std::string name, const int & score) {
    LoadScoreFile();
	
    std::ofstream f(GameData::m_ScorePath, std::ofstream::out | std::ofstream::trunc);
    int counter = 0;
    bool saveFlag = false;
    auto itN = m_Names.begin();
    auto itS = m_Scores.begin();
    std::string tmp;
    //Saves top scores
    while (itN!=m_Names.end()&&itS!=m_Scores.end() && counter < GameData::m_TopScore){
        if(!saveFlag && *itS< score){
            f<<name<<std::endl;
            f<<score<<std::endl;
            saveFlag = true;
            counter++;
        }
		if(counter < GameData::m_TopScore){
			f<<*itN<<std::endl;
			f<<*itS<<std::endl;
		}
        itN++;
        itS++;
        counter++;
    }
    if(!saveFlag && counter < GameData::m_TopScore){
        f<<name<<std::endl;
        f<<std::to_string(score)<<std::endl;
    }
    f.close();
}

void ScoreManager::LoadScoreFile() {
    if(!m_Names.empty()){
        m_Names.clear();
        m_Scores.clear();
    }
    std::string line;
    std::ifstream f(GameData::m_ScorePath,std::fstream::in);
    int counter = 0;
    //parses and loads scores, names to buffer
    while(getline(f, line)) {
        if(line=="" || line == "\r"){
            continue;
        }
        if(counter%2==0){
            m_Names.push_back(line);
        }
        else{
            m_Scores.push_back(Parseline(line));
        }
        counter++;
    }
    f.close();
}

int ScoreManager::Parseline(const std::string & line){
    auto it = line.begin();
    std::string parsed;
    for (;it!= line.end();it++){
    	if(isdigit((int)*it)){
    		parsed.push_back(*it);
    	}
    	else if( *it == '\r'){
    		break;
    	}
    	else{
    		return 0;
    	}
    }
    return std::stoi(parsed);
}
