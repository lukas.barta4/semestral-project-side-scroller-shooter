#ifndef ScoreManager_H
#define ScoreManager_H
#include <list>
#include <string>
/**
 * @brief Mayer singlton class of ScoreManager which saves and loads scores
 */
class ScoreManager{
public:
    /**
     * @brief Get the Instance object
     * 
     */
    static ScoreManager& GetInstance();
    /**
     * @brief Prints top scores
     * 
     */
    void PrintScore();
    /**
     * @brief Saves score to a file
     * 
     * @param name string name of player
     * @param score int score
     */
    void SaveScore(std::string name, const int & score);
private:
    /**
    * @brief Private constructor to achiave singleton pattern
    * 
    */
    ScoreManager(){}
    /**
     * @brief Parses line and returns parsed number
     * 
     * @param line input line to parse
     * @return int parsed int
     */
    int Parseline(const std::string & line);
    /**
     * @brief Loads score from file into buffer
     * 
     */
    void LoadScoreFile();
    std::list<std::string> m_Names; /**< Name buffer*/
    std::list<int> m_Scores; /**< Score buffer*/
};

#endif /* ScoreManager_H */