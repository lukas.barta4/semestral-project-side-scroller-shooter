#ifndef SpawnData_H
#define SpawnData_H
#include <string>
/**
 * @brief Struct to store data from level file
 * 
 */
struct SpawnData{
    int m_SpawnTime;
    int m_SpawnYPos;
    int m_Type;
};

#endif /* SpawnData_H */