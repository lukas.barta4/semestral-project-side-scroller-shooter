#include "Weapon.h"
#include "GameScene.h"
#include "GameData.h"
#include "Projectile.h"

Weapon::Weapon(const int &x, const int &y):m_X(x),m_Y(y) {
    m_CoolDownTime = std::chrono::system_clock::now();
    m_StateSwitch = false;
    m_Level = 1;
}

void Weapon::SetLevel(const int & level) {
    m_Level = level;
}

void Weapon::Upgrade(){
    if(m_Level < GameData::m_MaxWeaponLevel){
        m_Level++;
    }
}

void Weapon::SetFireRate(const int & rate) {
    m_FireRate = rate;
}

void Weapon::SetDirection(const int & dirx) {
    m_FireDirX = dirx;
}

void Weapon::SetOffsetXY(const int & x, const int & y){
    m_OffsetX = x;
    m_OffsetY = y;
}


int Weapon::GetLevel()const{
    return m_Level;
}
void Weapon::Update(){
    std::chrono::system_clock::duration deltaTime = std::chrono::system_clock::now() - m_CoolDownTime;
    int deltaMillis = std::chrono::duration_cast<std::chrono::milliseconds>(deltaTime).count();
    //check if cooldown has expired
    if (60000/m_FireRate <= deltaMillis){
        Fire();
        m_CoolDownTime = std::chrono::system_clock::now();
    }
}

RocketLauncher::RocketLauncher(const int & x, const int & y):Weapon(x,y) {
    m_FireRate = GameData::m_RocketLauncherFireRate;
}

int RocketLauncher::GetType()const{
    return m_Type;
}

void RocketLauncher::Fire() {
    switch (m_Level){
        case 1:
            GameScene::GetInstance().AddObject(new Rocket(m_X+m_OffsetX + m_FireDirX,m_Y+m_OffsetY,m_FireDirX,0));
            break;
        case 2:
            if(m_StateSwitch){
                GameScene::GetInstance().AddObject(new Rocket(m_X+m_OffsetX + m_FireDirX,m_Y+m_OffsetY,m_FireDirX,0));
                m_StateSwitch = false;
            }
            else{
                GameScene::GetInstance().AddObject(new MiniRocket(m_X+m_OffsetX,m_Y+m_OffsetY - 2,m_FireDirX,0));
                GameScene::GetInstance().AddObject(new MiniRocket(m_X+m_OffsetX,m_Y+m_OffsetY + 2,m_FireDirX,0));
                m_StateSwitch = true;
            }
            break;
        default:
            GameScene::GetInstance().AddObject(new MiniRocket(m_X+m_OffsetX,m_Y+m_OffsetY - 2,m_FireDirX,0));
            GameScene::GetInstance().AddObject(new Rocket(m_X+m_OffsetX + m_FireDirX,m_Y+m_OffsetY,m_FireDirX,0));
            GameScene::GetInstance().AddObject(new MiniRocket(m_X+m_OffsetX,m_Y+m_OffsetY + 2,m_FireDirX,0));
            break;
    }
}

MachineGun::MachineGun(const int & x, const int & y):Weapon(x,y) {
    m_FireRate = GameData::m_MachineGunFireRate;
}

int MachineGun::GetType()const{
    return m_Type;
}

void MachineGun::Fire() {
    switch (m_Level){
        case 1:
            GameScene::GetInstance().AddObject(new Bullet(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,0));
            break;
        case 2:
            if(m_StateSwitch){
                GameScene::GetInstance().AddObject(new Bullet(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,0));
                m_StateSwitch = false;
            }
            else{
                GameScene::GetInstance().AddObject(new Bullet(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,1));
                GameScene::GetInstance().AddObject(new Bullet(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,-1));
                m_StateSwitch = true;
            }
            break;
        default:
            GameScene::GetInstance().AddObject(new Bullet(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,1));
            GameScene::GetInstance().AddObject(new Bullet(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,0));
            GameScene::GetInstance().AddObject(new Bullet(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,-1));
            break;
    }
}

Cannon::Cannon(const int & x, const int & y):Weapon(x,y) {
    m_FireRate = GameData::m_CannonFireRate;
}

int Cannon::GetType()const{
    return m_Type;
}

void Cannon::Fire() {
    switch (m_Level){
        case 1:
            GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,0));
            break;
        case 2:
            if(m_StateSwitch){
                GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,0));
                m_StateSwitch = false;
            }
            else{
                GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,0));
                GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,1));
                GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,-1));
                m_StateSwitch = true;
            }
            break;
        default:
            GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,1));
            GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,0));
            GameScene::GetInstance().AddObject(new Shell(m_X+m_OffsetX,m_Y+m_OffsetY,m_FireDirX,-1));
            break;
    }
}
