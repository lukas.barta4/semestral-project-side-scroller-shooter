#ifndef Weapon_H
#define Weapon_H
#include <chrono>



class GameScene;
/**
 * @brief Factory styled class for creating projectiles
 * 
 */
class Weapon{
public:
    /**
     * @brief Construct a new Weapon object with coordinate refferences to owner of weapon
     * 
     */
    Weapon(const int &x, const int &y);
	virtual ~Weapon(){}
    /**
     * @brief Set the Level of a weapon
     * 
     * maximum level is 3
     */
    void SetLevel(const int & level);
    /**
     * @brief Increases weapon level by one if possible
     * 
     */
    void Upgrade();
    /**
     * @brief Set the Fire Rate object
     * 
     * @param rate int projectiles per minute
     */
    void SetFireRate(const int & rate);
    /**
     * @brief Set horizontal movement direction
     * 
     */
    void SetDirection(const int & dirx);
    /**
     * @brief Set the firing offset relative to weapon owner
     * 
     */

    void SetOffsetXY(const int & x, const int & y);

    virtual int GetType()const = 0;
    int GetLevel()const;

    /**
     * @brief Fires projectile after cooldown has run out
     * 
     */
    void Update();
    
protected:
    /**
     * @brief Pure virtual function to create projectiles
     * 
     */
    virtual void Fire() =0; 
    int m_FireRate; /**< projectiles per minute */
    std::chrono::time_point<std::chrono::system_clock> m_CoolDownTime; /**< Cooldown clock */
    int m_Level;
    
    int m_FireDirX;
    int m_OffsetX;
    int m_OffsetY;
    const int &m_X; /**< X posistion of weapon owner */
    const int &m_Y; /**< Y posistion of weapon owner */
    static const int m_Type;
    bool m_StateSwitch; /**< Firing state switch */
};

/**
 * @brief RocketLauncher balanced weapon with direct fire
 * 
 */
class RocketLauncher : public Weapon{
public:
    /**
     * @brief Construct a new Rocket Launcher object with coordinate refferences to owner of weapon
     * 
     */
    RocketLauncher(const int &x, const int &y);
    int GetType()const override;
    static const int m_Type = 0;
protected:
    void Fire()override;
    
};

/**
 * @brief MachineGun starter weapon with ability to bounce bullets off borders
 * 
 */
class MachineGun : public Weapon{
public:
    /**
     * @brief Construct a new Machine Gun object with coordinate refferences to owner of weapon
     * 
     */

    MachineGun(const int &x, const int &y);
    int GetType()const override;
    static const int m_Type = 1;
protected:
    void Fire()override;
};

/**
 * @brief Cannon slow firing high damage weapon
 * 
 */
class Cannon : public Weapon{
public:
    /**
    * @brief Construct a new Cannon object with coordinate refferences to owner of weapon
     * 
     */
    Cannon(const int &x, const int &y);
    int GetType()const override;
    static const int m_Type = 2;
protected:
    void Fire()override;
};

#endif /* Weapon_H */