
#include "Menu.h"

/**
 * @brief Starts the program
 */

int main ( void ){
    Menu * menu = new Menu();
    menu->StartMenu();
    delete menu;
    return 0;
    
}

